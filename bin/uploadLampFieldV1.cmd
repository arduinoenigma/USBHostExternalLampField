@echo off
set enigmaname=ExternalLampFieldV1.hex

if not "%1"=="" goto process 

echo.
echo usage:
echo.
echo %0 NN
echo.
echo where NN is the numeric part of the COM ports listed below
echo for example, for COM4 type:
echo.
echo %0 4
echo.
echo list of ports:
echo.
\Windows\system32\mode | \Windows\system32\find "COM"

goto :exit

:process

set avrpath=%PROGRAMFILES%\Arduino\hardware\tools\avr\
if exist "%avrpath%bin\avrdude.exe" goto process

set avrpath=%PROGRAMFILES(x86)%\Arduino\hardware\tools\avr\
if exist "%avrpath%bin\avrdude.exe" goto process

echo.
echo Cannot find AVRDUDE.EXE, ensure Arduino IDE is installed
echo If the Arduino IDE is installed, copy this folder
echo to the same drive i.e: C:\ or D:\ that contains your Arduino installation

goto :exit

:process

"%avrpath%bin\avrdude" -C"%avrpath%etc\avrdude.conf" -v -p atmega328p -c arduino -P COM%1 -b 57600 -D -Uflash:w:"%enigmaname%":i

:exit

echo.
echo %0 done
