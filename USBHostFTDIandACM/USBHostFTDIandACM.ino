#include <cdcftdi.h>
#include <cdcacm.h>
#include <usbhub.h>
#include <SPI.h>

#define USBDevBaud 9600

class FTDIAsync : public FTDIAsyncOper
{
  public:
    uint8_t OnInit(FTDI *pftdi);
};

uint8_t FTDIAsync::OnInit(FTDI *pftdi)
{
  uint8_t rcode = 0;

  rcode = pftdi->SetBaudRate(9600);

  rcode = pftdi->SetFlowControl(FTDI_SIO_DISABLE_FLOW_CTRL);

  return rcode;
}

class ACMAsyncOper : public CDCAsyncOper
{
  public:
    uint8_t OnInit(ACM *pacm);
};

uint8_t ACMAsyncOper::OnInit(ACM *pacm)
{
  uint8_t rcode;

  // originally set to 3 for DTR=1 RTS=1
  //            set to 1 for DTR=0 RTS=1
  // set DTR=0 RTS=1 to avoid resetting the simulator when plugged in
  rcode = pacm->SetControlLineState(1); //set to 3 for DTR = 1 RTS=1 and (1) for DTR=0 RTS=1

  if (rcode)
  {
    return rcode;
  }

  LINE_CODING  lc;
  lc.dwDTERate  = USBDevBaud;
  lc.bCharFormat  = 0;
  lc.bParityType  = 0;
  lc.bDataBits  = 8;

  rcode = pacm->SetLineCoding(&lc);

  return rcode;
}

USB              Usb;
//USBHub         Hub(&Usb);

FTDIAsync        FtdiAsync;
FTDI             Ftdi(&Usb, &FtdiAsync);

ACMAsyncOper  AsyncOper;
ACM           Acm(&Usb, &AsyncOper);

void setup()
{
  Serial.begin( 9600 );
  Serial.println("Start");

  if (Usb.Init() == -1)
    Serial.println("OSC did not start.");

  Serial.println("Started");

  delay( 200 );
}

void loop()
{
  Usb.Task();

  //Serial.println(Usb.getUsbTaskState());

  if ( Usb.getUsbTaskState() == USB_STATE_RUNNING )
  {
    uint8_t  rcode;
    //Serial.print(".");

    uint8_t  buf[64];

    for (uint8_t i = 0; i < 64; i++)
      buf[i] = 0;

    uint16_t rcvd = 64;
    rcode = Ftdi.RcvData(&rcvd, buf);

    if (rcvd > 2)
      Serial.print((char*)(buf + 2));

    // process commands comming from USB Host board
    if (Acm.isReady())
    {
      // buffer size must be greater or equal to max.packet size
      // it it set to 64 (largest possible max.packet size) here
      // can be tuned down for particular endpoint
      uint8_t  buf[64];
      uint16_t rcvd = 64;
      rcode = Acm.RcvData(&rcvd, buf);

      if ( rcvd )
      {
        for (uint16_t i = 0; i < rcvd; i++ ) {
          Serial.print((char)buf[i]);
          //Analyze((char)buf[i]);
        }
      }
    }

    delay(10);
  }
}