// upload this code to an Arduino Uno or a Genuine Arduino Nano (FTDI chip) and plug into a lampfield to illuminate all letters in sequence
//
// sends er>er><letter> to light up a letter in the lamp field, waits for one second and then sends CRLF to blank it.
// if a ~ is received, LED13 is lit, indicating a successful link to the lamp field.
// measures the time delay between sendind er>er>X and receiving a ~ from the lamp field
// once A..Z are lit, the measured time delay is shown by flashing a letter three times, then starts lighting up A..Z again
// A = 0 ms (most likely a 255 overflow)
// B = 10 ms
// C = 20 ms
// D = 30 ms
// ...
//
// Currently it flashes
// D for FTDI devices.
// C for ATmega16U2 devices

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
}

char c = 'A';
byte dt = 10;

void loop() {
  // put your main code here, to run repeatedly:
  char d = 0;

  // reset the state machine on the lamp field by sending it CRLF, then send it the sequence to light up a letter
  Serial.println(F(""));
  Serial.print(F("er>er>"));
  Serial.print(c);

  // if Z..A transition, light up the measured delay 3 times, A=smaller delay Z=largest delay
  c++;
  if (c > 'Z') {
    c = 'A';

    for (byte i = 0; i < 3; i++) {
      delay(1000);
      Serial.println(F(""));
      delay(500);
      Serial.print(F("er>er>"));
      Serial.print((char)('A' + (dt / 10)));
    }
  }

  // wait a fixed amount of time for the lampfield and see if we got a response ~
  delay(dt);
  if (Serial.available()) {
    d = Serial.read();
  }

  // clear any other characters waiting in the buffer
  for (byte i = 0; i < 3; i++)
  {
    while (Serial.available()) {
      Serial.read();
      delay(1);
    }
    delay(1);
  }

  // if we receive a tilde, light up LED13, otherwise turn it off, serves as link indicator
  if (d == '~')
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
    dt += 10;
  }

  // keep the last letter on for 1 second, adjust for processing delay by subtracting it
  delay(1000 - dt);
}