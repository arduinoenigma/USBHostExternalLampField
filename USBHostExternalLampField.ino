//-----------------------
// Lamp Field now connects to ATmega16U2 devices (UNO/Mega) and FTDI devices (Nano)
//
// Order the lamp field board at https://oshpark.com/shared_projects/CHp3OJUL
// square leg of 6 pin header goes into Gravitech Shield Pin A5, header covers A0..A5
//
// Order a Gravitech USBHOST add-on for Arduino Nano http://www.gravitech.us/usadforarna.html
// using solder, bridge all three pads, VIN, GPX, INT
//
// Order an Arduino Nano http://www.gravitech.us/arna30wiatp.html
//
// Download and install https://github.com/felis/USB_Host_Shield_2.0
//
// Download and install https://github.com/bhagman/SoftPWM
//
// Locate the following line in Libraries\Documents\Arduino\libraries\USB_Host_Shield_2.0-master\UsbCore.h
//
// typedef MAX3421e<P10, P9> MAX3421E;
//
// Change to
//
// typedef MAX3421e<P8, P2> MAX3421E;     // SS @ D8 (pin11)  INT @ D2 (pin5)
//

// parameter 8 : er#8? and er#8200 is brightness
// parameter 9 : er#9? and er#9126 is inhibit character 126 (~) for all and (@) for printer only

//-----------------------
#define USBDevBaud 9600

//#define USBResetPin 3
//#define USBGPXPin 7
//#define USBIntPin 2

#include <cdcftdi.h>
#include <cdcacm.h>
#include <usbhub.h>
#include <SPI.h>

#include "USBInit.h"

//----------------------

#include "Adafruit_Thermal.h"

#include "SoftwareSerial.h"
#define TX_GND 4 // Ground reference  BLACK WIRE   labeled GND on printer
#define TX_PIN 5 // Arduino transmit  YELLOW WIRE  labeled RX on printer
#define RX_PIN 6 // Arduino receive   GREEN WIRE   labeled TX on printer

SoftwareSerial mySerial(RX_PIN, TX_PIN); // Declare SoftwareSerial obj first
Adafruit_Thermal printer(&mySerial);     // Pass addr to printer constructor
// Then see setup() function regarding serial & printer begin() calls.

//----------------------

#include <SoftPWM.h>
#include <SoftPWM_timer.h>

//----------------------

#include <avr/pgmspace.h>
#include <EEPROM.h>
#include <avr/eeprom.h>

//Get ENIGMADATASIZE with Serial.println(sizeof(lampfieldData_t));
#define LAMPFIELDDATASIZE 13
#define EEPROMADD 10
#define EEPROMADD1 (EEPROMADD + LAMPFIELDDATASIZE)

struct lampfieldData_t
{
  byte Init1;
  unsigned char Ver1;

  byte heatDots;
  byte heatTime;
  byte heatInterval;
  byte printDensity;
  byte printBreakTime;
  byte dotPrintTime;
  byte dotFeedTime;

  byte PWMDuty;

  byte inhibitChar;

  unsigned char Ver2;
  byte Init2;
}
LampFieldData;

enum usbDriver {
  usbNONE,
  usbACM,
  usbFTDI,
};

usbDriver CurrentUSBDriver = usbNONE;

//----------------------

#define AUTOSAVEDELAY 4000
unsigned long autoSaveTimer = 0;
byte autoSaveDone = 0;

enum blinkStateMachine {
  BIDLE,
  BOFF,
  BON,
};

char LastK = 0;
byte blinkKey = 0;
unsigned long blinkTimer = 0;

//----------------------

#define MorsePIN 10
#define PINON 0
#define PINOFF 1

//----------------------

void LightOn()
{
  digitalWrite(MorsePIN, PINON);
}

void LightOff()
{
  digitalWrite(MorsePIN, PINOFF);
}

//-----------------------

void setup()
{

  Serial.begin(9600);

  //invalidate eeprom data to force readeeprom to init it
  //LampFieldData.Init2=0;
  //WriteEEPROM();

  ReadEEPROM();

  SoftPWMBegin();

  pinMode(MorsePIN, OUTPUT);
  LightOff();
  AllOff();

  //black into D4
  //yellow into D5
  //green into D6
  pinMode(TX_GND, OUTPUT); digitalWrite(TX_GND, LOW);
  mySerial.begin(19200);        // Initialize SoftwareSerial

  InitPrinter();

  //  pinMode(USBResetPin, OUTPUT);
  //  pinMode(USBGPXPin, INPUT);
  //  pinMode(USBIntPin, INPUT);

  //  digitalWrite(USBResetPin, 0);
  //  delay(100);
  //  digitalWrite(USBResetPin, 1);

  if (Usb.Init() == -1)
  {
    Serial.println(F("USB Init Failed"));
    do {
      delay(100);
      LightKey('E');
      delay(100);
      AllOff();
    } while (Usb.Init() == -1);
  }

  delay(100);

  //LampCheck();
  //LampCheck();
  //LampCheck();
}

void loop()
{
  uint8_t rcode;

  // buffer size must be greater or equal to max.packet size
  // it it set to 64 (largest possible max.packet size) here
  // can be tuned down for particular endpoint
  uint8_t  buf[64];
  uint16_t rcvd;

  Usb.Task();

  for (uint8_t i = 0; i < 64; i++)
    buf[i] = 0;
  rcvd = 64;
  rcode = Ftdi.RcvData(&rcvd, buf);

  // FTDI driver returns 64 characters if plugged once then unplugged
  if ((rcvd > 2) && (rcvd < 64))
  {
    CurrentUSBDriver = usbFTDI;

    //Serial.println(rcvd);

    for (uint16_t i = 2; i < rcvd; i++ ) {
      //Serial.print(i);
      Analyze((char)buf[i]);
    }
    //Serial.println("");
  }

  // process commands comming from USB Host board
  if (Acm.isReady())
  {
    for (uint8_t i = 0; i < 64; i++)
      buf[i] = 0;
    rcvd = 64;
    rcode = Acm.RcvData(&rcvd, buf);

    if ( rcvd )
    {
      CurrentUSBDriver = usbACM;

      //Serial.println(rcvd);

      for (uint16_t i = 0; i < rcvd; i++ ) {
        //Serial.print(i);
        Analyze((char)buf[i]);
      }
      //Serial.println("");

      //to send:
      //uint8_t data = '!';
      //rcode = Acm.SndData(1, &data);
    }
  }

  // also process commands coming from built in serial port
  // no collision logic is implemented, state machine will react to both simultaneously
  // ok as long as only one command comes in through one interface at a time

  if (Serial.available()) {
    Analyze(Serial.read());
  }

  // if there is anything queued to send out, send it, call often
  SendMorse();
  MininumLightTime();
  AutoSaveParameters();
  BlinkKey();
}
