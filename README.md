# USBHostExternalLampField
USBHostExternalLampField

This sketch uses the Gravitech USB Host 4 Nano Shield and an Arduino Nano to drive a charlieplexed external lamp field, flash enigma encoded Morse code and control a thermal printer

main file is: USBHostExternalLampField.ino

Hardware used:

USBHOST add-on for Arduino Nano: http://www.gravitech.us/usadforarna.html
Using a soldering iron, bridge the VIN, INT and GPX solder pads. Connect a 9V battery to the screw terminals in the shield, this will also power the USB port and any attached devices.

Arduino Nano: http://www.gravitech.us/arna30wiatp.html

Demo video: https://www.instagram.com/p/BXVK_BChVLh/

Software:

Download and install https://github.com/bhagman/SoftPWM

Download and install https://github.com/felis/USB_Host_Shield_2.0

 Locate the following line in Libraries\Documents\Arduino\libraries\USB_Host_Shield_2.0-master\UsbCore.h

 typedef MAX3421e<P10, P9> MAX3421E;

 Change to

 typedef MAX3421e<P8, P2> MAX3421E;     // SS @ D8 (pin11)  INT @ D2 (pin5)
