byte PrintMaxLine = 16;
byte PrintMaxGroup = 4;
byte PrintGroup = 0;
byte PrintLine = 0;
byte PrintedGroups = 0;

void InitPrinter()
{
  PrintGroup = 0;
  PrintLine = 0;

  // Printer performance may vary based on the power supply voltage,
  // thickness of paper, phase of the moon and other seemingly random
  // variables.  This method sets the times (in microseconds) for the
  // paper to advance one vertical 'dot' when printing and when feeding.
  // For example, in the default initialized state, normal-sized text is
  // 24 dots tall and the line spacing is 30 dots, so the time for one
  // line to be issued is approximately 24 * print time + 6 * feed time.
  // The default print and feed times are based on a random test unit,
  // but as stated above your reality may be influenced by many factors.
  // This lets you tweak the timing to avoid excessive delays and/or
  // overrunning the printer buffer.

  // ESC 7 n1 n2 n3 Setting Control Parameter Command
  // n1 = "max heating dots" 0-255 -- max number of thermal print head
  //      elements that will fire simultaneously.  Units = 8 dots (minus 1).
  //      Printer default is 7 (64 dots, or 1/6 of 384-dot width), this code
  //      sets it to 11 (96 dots, or 1/4 of width).
  // n2 = "heating time" 3-255 -- duration that heating dots are fired.
  //      Units = 10 us.  Printer default is 80 (800 us), this code sets it
  //      to value passed (default 120, or 1.2 ms -- a little longer than
  //      the default because we've increased the max heating dots).
  // n3 = "heating interval" 0-255 -- recovery time between groups of
  //      heating dots on line; possibly a function of power supply.
  //      Units = 10 us.  Printer default is 2 (20 us), this code sets it
  //      to 40 (throttled back due to 2A supply).
  // More heating dots = more peak current, but faster printing speed.
  // More heating time = darker print, but slower printing speed and
  // possibly paper 'stiction'.  More heating interval = clearer print,
  // but slower printing speed.

  // Print density description from manual:
  // DC2 # n Set printing density
  // D4..D0 of n is used to set the printing density.  Density is
  // 50% + 5% * n(D4-D0) printing density.
  // D7..D5 of n is used to set the printing break time.  Break time
  // is n(D7-D5)*250us.
  // (Unsure of the default value for either -- not documented)

  //#define printDensity   10 // 100% (? can go higher, text is darker but fuzzy)
  //#define printBreakTime  2 // 500 uS

  //dotPrintTime   = 30000; // See comments near top of file for
  //dotFeedTime    =  2100; // an explanation of these values.

  //begin(uint8_t heatDots=11, uint8_t heatTime=120, uint8_t heatInterval=40, uint8_t printDensity=10, uint8_t printBreakTime=2),
  //setTimes(unsigned long p=30, unsigned long f=21) // 30*1000 , 21*100

  LampFieldData.dotPrintTime;
  LampFieldData.dotFeedTime;

  //printer.begin(1, 120, 40, 10, 2);          // Init printer (same regardless of serial type) (heat time)
  printer.begin(LampFieldData.heatDots, LampFieldData.heatTime, LampFieldData.heatInterval, LampFieldData.printDensity, LampFieldData.printBreakTime);
  printer.setTimes(LampFieldData.dotPrintTime * 1000,  LampFieldData.dotFeedTime * 100); // (print time, feed time)
  printer.setSize('L');        // Set type size, accepts 'S', 'M', 'L'
}

void SetPrinterGroups(byte groups)
{
  PrintMaxGroup = groups;

  //Serial.print(F("[PRINTER:"));
  //Serial.print(PrintMaxGroup);
  //Serial.println(F("]"));
}

void PrinterFeed()
{
  //Serial.println(F("[PRINTER FEED]"));
  PrintGroup = 0;
  PrintLine = 0;
  PrintedGroups = 0;

  printer.feed(2);
}

void Print(char c)
{
  //Serial.print(c);
  printer.print(c);

  PrintGroup++;
  PrintLine++;

  if (PrintLine == PrintMaxLine)
  {
    PrinterFeed();
  }
  else if (PrintGroup == PrintMaxGroup)
  {
    //Serial.print(F(" "));
    printer.print(F(" "));
    PrintGroup = 0;
    PrintedGroups++;
  }

  if (((PrintedGroups == 2) && ((PrintMaxGroup == 5) || (PrintMaxGroup == 6))) || ((PrintedGroups == 3) && (PrintMaxGroup == 4)))
  {
    PrinterFeed();
  }
}
