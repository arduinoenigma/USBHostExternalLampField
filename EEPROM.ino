void ReadEEPROM()
{
  lampfieldData_t LampFieldData1;

  // read eeprom data
  eeprom_read_block((void*)&LampFieldData, (void*)EEPROMADD, LAMPFIELDDATASIZE);               // load first block onto working structure
  eeprom_read_block((void*)&LampFieldData1, (void*)EEPROMADD1, LAMPFIELDDATASIZE);             // and second block onto temporary structure

  if ((unsigned char)(LampFieldData.Ver2 - LampFieldData.Ver1) != (unsigned char)1)            // if first block is corrupted
  {
    eeprom_read_block((void*)&LampFieldData, (void*)EEPROMADD1, LAMPFIELDDATASIZE);            // load second block onto working structure
    eeprom_write_block((const void*)&LampFieldData, (void*)EEPROMADD, LAMPFIELDDATASIZE);      // and save working structure into first block
  }

  if ((unsigned char)(LampFieldData1.Ver2 - LampFieldData1.Ver1) != (unsigned char)1)          // if second block is corrupted
  {
    eeprom_write_block((const void*)&LampFieldData, (void*)EEPROMADD1, LAMPFIELDDATASIZE);     // save working structure into second block
  }

  // initialize to default values if data seems corrupted
  if ((LampFieldData.Init1 != 5) || (LampFieldData.Init2 != 7))
  {
    Serial.println("init eeprom data");

    LampFieldData.Init1 = 5;
    LampFieldData.Init2 = 7;

    LampFieldData.Ver1 = 0;
    LampFieldData.Ver2 = 1;

    LampFieldData.heatDots = 1;
    LampFieldData.heatTime = 120;
    LampFieldData.heatInterval = 40;
    LampFieldData.printDensity = 10;
    LampFieldData.printBreakTime = 2;
    LampFieldData.dotPrintTime = 30; // *1000
    LampFieldData.dotFeedTime = 21; // *100

    LampFieldData.PWMDuty = 255;

    LampFieldData.inhibitChar = '~';              // ~ to disable lampfield and printer   @ to disable printer

    WriteEEPROM();
  }
}


void WriteEEPROM()
{
  LampFieldData.Ver1 = LampFieldData.Ver1 + 1;
  LampFieldData.Ver2 = LampFieldData.Ver1 + 1;

  eeprom_write_block((const void*)&LampFieldData, (void*)EEPROMADD, LAMPFIELDDATASIZE);
  eeprom_write_block((const void*)&LampFieldData, (void*)EEPROMADD1, LAMPFIELDDATASIZE);
}

byte getParameter(char param)
{
  byte v = 0;
  switch (param)
  {
    case '1':
      v = LampFieldData.heatDots;
      break;

    case '2':
      v = LampFieldData.heatTime;
      break;

    case '3':
      v = LampFieldData.heatInterval;
      break;

    case '4':
      v = LampFieldData.printDensity;
      break;

    case '5':
      v = LampFieldData.printBreakTime;
      break;

    case '6':
      v = LampFieldData.dotPrintTime;
      break;

    case '7':
      v = LampFieldData.dotFeedTime;
      break;

    case '8':
      v = LampFieldData.PWMDuty;
      break;

    case '9':
      v = LampFieldData.inhibitChar;
      break;
  }

  return v;
}

void setParameter(char param, byte pvalue)
{
  switch (param)
  {
    case '1':
      LampFieldData.heatDots = pvalue;
      break;

    case '2':
      LampFieldData.heatTime = pvalue;
      break;

    case '3':
      LampFieldData.heatInterval = pvalue;
      break;

    case '4':
      LampFieldData.printDensity = pvalue;
      break;

    case '5':
      LampFieldData.printBreakTime = pvalue;
      break;

    case '6':
      LampFieldData.dotPrintTime = pvalue;
      break;

    case '7':
      LampFieldData.dotFeedTime = pvalue;
      break;

    case '8':
      LampFieldData.PWMDuty = pvalue;
      break;

    case '9':
      LampFieldData.inhibitChar = pvalue;
      break;
  }
}

void AutoSaveParameters()
{
  if (autoSaveTimer)
  {
    autoSaveTimer--;

    if (autoSaveTimer == 0)
    {
      WriteEEPROM();

      autoSaveDone = 1;
      blinkKey = 1;
    }
  }
}


void BlinkKey()
{
  static blinkStateMachine BlinkSM = BIDLE;

  switch (BlinkSM)
  {
    case BIDLE:

      if (blinkKey)
      {
        blinkKey = 0;

        if (LastKey != 0)
        {
          LastK = LastKey;
          AllOff();

          blinkTimer = 1000;
          BlinkSM = BOFF;
        }

        break;

      case BOFF:

        if (blinkTimer) {
          blinkTimer--;
        }
        else
        {
          BlinkSM = BON;
        }

        break;

      case BON:

        if ((LastKey != 0) && (LastKey != LastK))
        {
          LastK = LastKey;
        }

        LightKey(LastK);
        BlinkSM = BIDLE;

        break;
      }
  }
}
